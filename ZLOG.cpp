//Created by David Tran (unsignedzero)
//for the simple purpose of controlling Logs
//Version 0.1.1.1

#ifndef ZLOG_CPP
 #define ZLOG_CPP true

#ifndef ZLOG_H
 #include "ZLOG.H"
 #ifndef ZLOG_H
  #error ZLOG.H does not exist.\
   Unknown Function Delcations
 #endif
#endif

namespace zx{

  //Edit
  bool zlog::file( std::string in ){
    flog.open(in.c_str());
    flog ? bstatus = true : bstatus = false;
    return (bstatus);
  }

  bool zlog::file( std::string in , int inn ){
    if ( inn==0 ) { flog.open(in.c_str() , std::fstream::app ); }
    if ( inn==1 ) { flog.open(in.c_str() , std::fstream::app | std::ios::binary); }
    flog ? bstatus = true : bstatus = false;
    return (bstatus);
  }

  void zlog::write(){
    if ( bstatus ) {
      if ( buffer.str() != "" ) {
        flog << (buffer.str());
        (buffer).str("");
        flog.flush();
        }
    #if(ZLOG_DUAL_BUFFER==true)
      if ( dbuffer.str() != "" ) {
        std::cout << (dbuffer.str());
       (dbuffer).str("");
        std::cout.flush();
        }
    #endif
    #if(ZLOG_WARNING==true)
      if (bstatus == false) {
        std::cout << ZLOG_CLI << "WARNING:NO file linked to ZLOG!\n";
        }
    #endif
    #if((ZLOG_WARNING==true)&&(ZLOG_NO_FILE_WRITE==true))
      if (bstatus == false){
        std::cout << ZLOG_CLI << "WARNING: Default File used for ZLOG!\n";
        bstatus=true;
        flog.open(ZLOG_DEFAULT_FNAME);
        write();
        }
    #endif
    }
  }

  //iterator
  //operator

  //observer

  std::string zlog::filename() const{
    return fname;
  }

  bool zlog::check() const{ return bstatus;}
  bool zlog::status() const { return bstatus; }

  #if(ZLOG_DUAL_BUFFER==false)
  unsigned int zlog::size() const{ return (buffer).str().length();}
  #endif
  #if(ZLOG_DUAL_BUFFER==true)
  unsigned int zlog::size() const{ return((buffer).str().length()+(dbuffer).str().length());}
  #endif

  unsigned int zlog::length() const{ return size();}

  void zlog::clear() {
    #if(ZLOG_WARNING==true)
      std::cout << ZLOG_CLI << "WARNING:Emptying ZLOG BUFFER!\n";
    #endif
    buffer.str("");
    #if(ZLOG_DUAL_BUFFER==true)
    dbuffer.str("");
    #endif
  }

  void zlog::printb() const{
    std::cout << buffer.str();
  }
  #if(ZLOG_DUAL_BUFFER==true)
  void zlog::printd() const{
    std::cout << dbuffer.str();
  }
  #endif

  //constructor
  zlog::zlog() {
    fname = '\0'; bstatus = false;}

  zlog::zlog( std::string in ){
    bstatus = file( in , 0 );}

  zlog::zlog( std::string in , int in2 ) {
    bstatus = file( in , in2 );}

   // Int 1

  zlog::zlog( int a ){
    if ( a == 0 ) {
      zlog();}
    else if ( a == 1 ) {
      //DO PROMPT
       do {
       std::cout << ZLOG_CLI << "Please enter a WORKING file name for the log file\n";
       std::cin >> fname;
       flog.open(fname.c_str());
       }while( !flog );
       bstatus = true;
      }
    else /*if (( a != 0 )&&(a != 1))*/ {
      std::cout << ZLOG_CLI << "Invalid Value! "
       << "WARNING:Default file used!\n";
      flog.open(ZLOG_DEFAULT_FNAME);
      bstatus = true;
      }
      std::cout << ZLOG_CLI << "File created...\n";
    }

  //deconstructor
  zlog::~zlog(){
    write();
    flog.close();
    }

}

#endif

