//Created by David Tran (unsignedzero)
//for the simple purpose of controlling Logs and output
//Version 0.1.1.1

#ifndef ZLOG_H
#define ZLOG_H true

#define ZLOG_CLI "ZLOG> "

#ifndef ZLOG_WARNING
 #define ZLOG_WARNING true
#endif
 //Displays warning messages

#ifndef ZLOG_NO_FILE_WRITE
 #define ZLOG_NO_FILE_WRITE true
#endif
 //Empties buffer to file IF none is set

#ifndef ZLOG_DEFAULT_FNAME
 #if(ZLOG_NO_FILE_WRITE==true)
  #define ZLOG_DEFAULT_FNAME "default.txt"
 #endif
#endif
 //Sets default dump file if above is true

#ifndef ZLOG_DUAL_BUFFER
 #define ZLOG_DUAL_BUFFER false
#endif
 //Creates a 2nd buffer to write to the screen

#if((ZLOG_DUAL_BUFFER!=false)&&(ZLOG_DUAL_BUFFER!=true))
 #error "BAD Define for ZLOG_DUAL_BUFFER"
#endif

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

namespace zx{

class zlog{

  public:

  //Edit
  bool file( std::string );
  bool file( std::string , int );

  void write();

  //iterator
  template <class type>
  void read(type let)
  #if(ZLOG_DUAL_BUFFER==false)
  { buffer << let; }
  #endif
  #if(ZLOG_DUAL_BUFFER==true)
  { buffer << let; dbuffer << let; }
  #endif

  #if(ZLOG_DUAL_BUFFER==true)
  template <class type>
  void read(type let, int mode)
  { mode == 1 ? dbuffer << let : buffer << let; }
  #endif

 #if(ZLOG_DUAL_BUFFER==true)
  template <class type, class dtype>
  void read(type let, dtype dlet)
  { dbuffer << dlet; buffer << let; }
  #endif

  template <class type>
   std::stringstream& operator<< (type let)
   #if(ZLOG_DUAL_BUFFER==false)
   { (buffer) << let; return( buffer ); }
   #endif
   #if(ZLOG_DUAL_BUFFER==true) ///TEST ZONE
   { (buffer) << let; (dbuffer) << let; return( buffer ); }
   #endif

  void clear();

  void flush();

  //operator

  //observer

  std::string filename() const;
  bool check() const;
  bool status() const;
  unsigned int size() const;
  unsigned int length() const;

  void printb() const;
  #if(ZLOG_DUAL_BUFFER==true)
  void printd() const;
  #endif
  //constructor

  zlog();
  explicit zlog( std::string );
  zlog( std::string , int);
  explicit zlog( int );

  //friend ostream &operator<< (ostream &, const log);

  //deconstructor
  ~zlog();

  private:

  std::ofstream flog;
  std::string fname;
  std::stringstream buffer;
  bool bstatus;

  #if(ZLOG_DUAL_BUFFER==true)
  std::stringstream dbuffer;
  #endif

  };

}

#ifndef ZLOG_CPP
 #include "ZLOG.cpp"
 #ifndef ZLOG_CPP
  #error ZLOG.CPP does not exist.\
   Unknown Function Implementation
 #endif
#endif

#endif

