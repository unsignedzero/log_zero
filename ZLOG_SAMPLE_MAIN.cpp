#define ZLOG_DUAL_BUFFER true
#include "Zlog.h"
#include <iostream>
#include <sstream>

int main() {

  bool test;

  zx::zlog flog(1);
  test = flog.status();

  std::stringstream blah;
  blah << "cjeeze" ;

  //std::cout << "Test value is " << test << '\n' << blah.str() << "12" << '\n';

  flog << "1x" << "2" << "the\n" << blah.str() << "12";
  flog.read("4345");
  flog.printb();

  flog.write();

  system("pause");

  return(0);
}

